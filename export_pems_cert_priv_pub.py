from Crypto.PublicKey import RSA
from sys import argv

print ('-------------------------------------------------------------------------------------------------------------')
print ('-------------------------------------------------------------------------------------------------------------')
print ('-------------------------------------------------------------------------------------------------------------')

#ARGUMENTOS RECEBIDOS VIA POPEN
path = argv[1]
certificado_token = argv[2]
#FIM ARGS
certificado_token = bytearray(certificado_token, 'utf8')
certificate_pem = "{}{}".format(path,'certificate.pem')
key_priv_pem = "{}{}".format(path,'key_priv.pem')
key_pub_pem = "{}{}".format(path,'key_pub.pem')

#EXPORTANDO CERTIFICADO
with open(certificate_pem, 'wb') as f:
    pem_bytes = certificado_token
    f.write(pem_bytes)

#EXPORTANDO KEY_PRIVADA e PUBLICA
key = RSA.generate(2048)
with open(key_priv_pem, 'wb') as f:
    pem_bytes = key.exportKey(format='PEM', passphrase='123456', pkcs=1)
    f.write(pem_bytes)

pub_key = key.publickey()
with open(key_pub_pem, 'wb') as f:
    pem_bytes = pub_key.exportKey('PEM')
    pem_bytes = pub_key.exportKey(format='PEM', passphrase='123456', pkcs=1)
    f.write(pem_bytes)

print ('-------------------------------------------------------------------------------------------------------------')
print ('-------------------------------------------------------------------------------------------------------------')
print ('-------------------------------------------------------------------------------------------------------------')
