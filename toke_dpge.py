import binascii
import getpass
import pkcs11
import subprocess
import sys
import types
from Crypto.PublicKey import RSA
from asn1crypto import pem
from asn1crypto.keys import RSAPublicKey
from pkcs11 import KeyType, ObjectClass, Mechanism, Attribute, MechanismFlag, MGF
from pkcs11.util.rsa import encode_rsa_public_key, decode_rsa_private_key
import io
import os
# import sys
# import chilkat
# pin = getpass.getpass('Informe seu PIN para logar no token:')
pin = 'Pcxs_1973' #Paulo Rh
# pin = '123456' #China Suporte

data = b'Data to sign'
arquivo_entrada = '/home/dpge/Downloads/VIVO_FATURA_SEGUNDA_VIA.pdf'
arquivo_saida = "{}.sig".format(arquivo_entrada)
arquivo_p7s = "{}.p7s".format(arquivo_entrada)

buffer_size = io.DEFAULT_BUFFER_SIZE
# crypt = chilkat.CkCrypt2()

def gera_id_aleatoria():
    import math
    from random import randint
    numero_aleatorio = (randint(0, 500))
    byte_count = int(math.log(numero_aleatorio, 256)) + 1
    hex_string = '{:0{}x}'.format(numero_aleatorio, byte_count * 2)
    return bytearray.fromhex(hex_string)

def get_platform():
    platforms = {
        'linux1': 'Linux',
        'linux2': 'Linux',
        'darwin': 'OS X',
        'win32': 'Windows'
    }
    if sys.platform not in platforms:
        sistema = sys.platform
    return sistema

def get_lib_token(sistema):
    lib_token = ''
    if sistema == 'linux':
        io = subprocess.Popen(['whereis', 'libeToken.so'], stdout=subprocess.PIPE).stdout
        for line in io:
            lib_token = line.rstrip().split()[1].decode("utf-8")
            lib_token = pkcs11.lib(lib_token)
    return lib_token

def criar_sessao_token(lib_token):
    try:
        for slot in lib_token.get_slots(token_present=True):
            token = slot.get_token()
            # print ("{}".format(token.slot))
            # print ("{} - {} - {} - {}".format(token.label, token.manufacturer_id, token.label, token.slot))
            session = token.open(user_pin=pin, rw=True)
            return session
    except pkcs11.TokenNotPresent:
        return 'Token nao esta inserido no computador'

def get_key_privada(session):
    #pegar chave privada
    priv_list = []
    for key_privada in session.get_objects({pkcs11.Attribute.CLASS: pkcs11.constants.ObjectClass.PRIVATE_KEY}):
        priv = key_privada
        priv_list.append(key_privada[Attribute.ID])
    return (priv, priv_list)

def criar_chave_publica_privada(session, priv_list, pub_list, id):
    public = ''
    private = ''
    if len(priv_list) < 1 and len(pub_list) < 1:
        public, private = session.generate_keypair(
            key_type=KeyType.RSA, key_length=2048, id=id, label='DEV_KEY_PYTHON',store=False,
            public_template={
                # Attribute.TOKEN: True,
                Attribute.PRIVATE: False,
                Attribute.LABEL: 'DEV_KEY_PYTHON',
                Attribute.ID: id,
                Attribute.MODULUS_BITS: 2048,
                Attribute.CLASS: ObjectClass.PUBLIC_KEY,
                Attribute.PUBLIC_EXPONENT: b'\1\0\1',
                Attribute.SENSITIVE: False,
                Attribute.EXTRACTABLE: True,
            },
            private_template={
                Attribute.TOKEN: True,
                Attribute.PRIVATE: True,
                Attribute.LABEL: 'DEV_KEY_PYTHON',
                Attribute.ID: id,
                Attribute.CLASS: ObjectClass.PRIVATE_KEY,
            }
        )
    return (public, private)

def get_key_publica(session, id):
    # pegar chave publica
    pub_list = []
    pub = ''
    for key_publica in session.get_objects({pkcs11.Attribute.CLASS: pkcs11.constants.ObjectClass.PUBLIC_KEY}):
        pub = key_publica

    if isinstance(pub, str):
        par_publica_privada = criar_chave_publica_privada(session, priv_list=[],pub_list=[], id=id)
        pub = par_publica_privada[0]
        pub_list.append(par_publica_privada[0][Attribute.ID])
    else:
        pub_list.append(key_publica[Attribute.ID])
    return (pub, pub_list)

def get_certificado(session):
    #pegar chave certificado
    from asn1crypto import pem, x509
    cert = next(session.get_objects({pkcs11.Attribute.CLASS: pkcs11.constants.ObjectClass.CERTIFICATE}))
    der_bytes = cert[pkcs11.Attribute.VALUE]
    cert = x509.Certificate.load(der_bytes)
    pem_bytes = pem.armor('CERTIFICATE', der_bytes)
    return pem_bytes

def get_extract_key_publica(key_publica):
    return RSA.importKey(encode_rsa_public_key(key_publica))

def assinando_dados(key_privada, data):
    return key_privada.sign(data)

def verifica_assinatura(key_publica, assinatura, data):
    return key_publica.verify(data, assinatura)

def encriptar_arquivo(arquivo_entrada, arquivo_saida, session, id):
    # Streaming File(Assinar)
    iv = session.generate_random(128)
    key_secret = session.generate_key(pkcs11.KeyType.AES, 256, label='DEV_KEY_LOCAL', id=id)
    with \
            open(arquivo_entrada, 'rb') as input_, \
            open(arquivo_saida, 'wb') as output:

        arquivo_buffer = iter(lambda: input_.read(buffer_size), '')

        for arquivo_encriptado in key_secret.encrypt(arquivo_buffer,
                                 mechanism_param=iv,
                                 buffer_size=buffer_size):
            return output.write(arquivo_encriptado)

def assinatura_arquivo(arquivo_saida, chaves_privada):
    signature = chaves_privada.sign(arquivo_saida,mechanism=Mechanism.RSA_PKCS_PSS,
                                    mechanism_param=(Mechanism.SHA_1,
                                    MGF.SHA1,
                                    20))
    return signature

def assinatura_p7s(arquivo):
    arquivo_assinado = '{}.sig'.format(arquivo)
    arquivo_a_envelopar = '{}.p7s'.format(arquivo)
    path_atual = "{}/".format(os.path.dirname(os.path.realpath(__file__)))
    private_key_pem = "{}{}".format(path_atual, 'privateKey.pem')
    certificate_pem = "{}{}".format(path_atual, 'caKey.pem')
    envelopar_arquivo_assinado = 'openssl smime -sign -binary -in {} -signer {} -inkey {} -outform DER -out {}'.format(
                                                                                          arquivo_assinado,
                                                                                          certificate_pem,
                                                                                          private_key_pem,
                                                                                          arquivo_a_envelopar)
    print (envelopar_arquivo_assinado)
    subprocess.Popen(envelopar_arquivo_assinado, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT,
                               close_fds=False)


def decode_pem_pub_priv():
    import Cryptodome
    # from Cryptodome import RSA, PublicKey
    key = RSA.generate(2048)
    pv_key_string = key.exportKey()
    with open("/home/dpge/key.pem", "w") as prv_file:
        print("{}".format(pv_key_string.decode()), prv_file)

    pb_key_string = key.publickey().exportKey()
    with open("/home/dpge/key_publica.pem", "w") as pub_file:
        print("{}".format(pb_key_string.decode()), pub_file)

# def decriptar_arquivo_assinado(arquivo_saida, chaves_privada):
#     # Streaming File(Assinar)
#     for arquivo_encriptado in chaves_privada.decrypt(arquivo_saida):
#             arquivo_encriptado

# decriptar_arquivo = chaves_privada.decrypt(arquivo_saida)
# print (decriptar_arquivo)
# Encode as ASN.1 for interchange
# signature = chaves_publicas(signature)

if __name__ == "__main__":
    sistema_cliente = get_platform()
    lib_token = get_lib_token(sistema_cliente)
    session = criar_sessao_token(lib_token)
    id_aleatoria = gera_id_aleatoria()
    if session:
        keys_lista_privada = get_key_privada(session)
        keys_lista_publica = get_key_publica(session, id=id_aleatoria)
        certificado = get_certificado(session)
        lista_chaves_privadas = keys_lista_privada[1]
        chaves_privadas = keys_lista_privada[0]
        lista_chaves_publicas = keys_lista_publica[1]
        par_publica_privada = criar_chave_publica_privada(session, priv_list=lista_chaves_privadas,
                                                          pub_list=lista_chaves_publicas, id=id_aleatoria)
        chaves_publicas = keys_lista_publica[0]
        extract_key_publica = get_extract_key_publica(key_publica=chaves_publicas)
        assinatura_dados = assinando_dados(key_privada=chaves_privadas, data=data)
        analisa_assinatura = verifica_assinatura(key_publica=chaves_publicas, assinatura=assinatura_dados, data=data)
        criptografar_arquivo = encriptar_arquivo(arquivo_entrada, arquivo_saida, session, id=id_aleatoria)
        assina_arquivo = assinatura_arquivo(arquivo_saida=arquivo_saida, chaves_privada=chaves_privadas)
        arquivo_p7s = assinatura_p7s(arquivo=arquivo_entrada)
        # decode_pub_priv = decode_pem_pub_priv()
        # arquivo_p7s = assinatura_p7s(session, chaves_privadas, arquivo_saida)
        # decriptar_arquivo = decriptar_arquivo_assinado(arquivo_saida=arquivo_saida, chaves_privada=chaves_privadas)

        print ("------------------------------------------------------------------")
        print ("------------------------------------------------------------------")
        print ("Sistema Usado pelo Cliente: {}".format(sistema_cliente))
        print ("------------------------------------------------------------------")
        print ("------------------------------------------------------------------")
        print ("Biblioteca usada do token: {}".format(lib_token))
        print ("------------------------------------------------------------------")
        print ("------------------------------------------------------------------")
        print ("Lista de Labels Chaves Privadas: {}".format(lista_chaves_privadas))
        print ("------------------------------------------------------------------")
        print ("------------------------------------------------------------------")
        print ("Lista de ID's Chaves Públicas: {}".format(lista_chaves_publicas[0].hex()))
        print ("------------------------------------------------------------------")
        print ("------------------------------------------------------------------")
        print ("Chaves Privadas: {}".format(chaves_privadas))
        print ("------------------------------------------------------------------")
        print ("------------------------------------------------------------------")
        print ("Chaves Públicas: {}".format(chaves_publicas))
        print ("------------------------------------------------------------------")
        print ("------------------------------------------------------------------")
        print ("Lista de Novos Pares de Chaves Públicas e Privadas: {}".format(par_publica_privada))
        print ("------------------------------------------------------------------")
        print ("------------------------------------------------------------------")
        print ("Chave Pública Exportada: {}".format(extract_key_publica))
        print ("------------------------------------------------------------------")
        print ("------------------------------------------------------------------")
        print ("Assinando Txt: {}".format(assinatura_dados))
        print ("------------------------------------------------------------------")
        print ("------------------------------------------------------------------")
        print ("Verifica Assinatura do Txt: {}".format(analisa_assinatura))
        print ("------------------------------------------------------------------")
        print ("------------------------------------------------------------------")
        print ("Encriptar Streaming File {}".format(criptografar_arquivo))
        print ("------------------------------------------------------------------")
        print ("------------------------------------------------------------------")
        print ("Assinar Arquivo Encriptado do Stream {}".format(assina_arquivo))
        print ("------------------------------------------------------------------")
        print ("------------------------------------------------------------------")
        print ("Assinatura P7S {}".format(arquivo_p7s))
        # print ("Decriptar Arquivo {}".format(decriptar_arquivo))
    else:
        print ('Token não está plugado no computador')
